﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CookieClicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , INotifyPropertyChanged
    {
        public int CookieCount { get; set; } = 0;

        public int FactoryCount { get; set; } = 0;
        public int FactoryTime { get; set; } = 0;
        public int FactoryCost { get; set; } = 10;
        public int FactoryCooldown { get; set; } = 500;
        public int FactoryMake { get; set; } = 1;

        public int MineCount { get; set; } = 0;
        public int MineTime { get; set; } = 0;
        public int MineCost { get; set; } = 100;
        public int MineCooldown { get; set; } = 800;
        public int MineMake { get; set; } = 25;

        public int PortalCount { get; set; } = 0;
        public int PortalTime { get; set; } = 0;
        public int PortalCost { get; set; } = 300;
        public int PortalCooldown { get; set; } = 1000;
        public int PortalMake { get; set; } = 300;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(Update);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0,0,1);
            dispatcherTimer.Start();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        // Default INotifyPropertyChanged
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        protected void UpdateWindow()
        {
            OnPropertyChanged(null);
        }


        #endregion

        private void Cookie_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CookieCount++;
            UpdateWindow();
        }

        private void FactoryBuy_Click(object sender, RoutedEventArgs e)
        {
            if (CookieCount >= FactoryCost)
            {
                FactoryCount++;
                CookieCount -= 10;
            }
        }

        private void MineBuy_Click(object sender, RoutedEventArgs e)
        {
            if (CookieCount >= MineCost)
            {
                MineCount++;
                CookieCount -= MineCost;
            }
        }

        private void PortalBuy_Click(object sender, RoutedEventArgs e)
        {
            if (CookieCount >= PortalCost)
            {
                PortalCount++;
                CookieCount -= PortalCost;
            }
        }

        private void Update(object sender, EventArgs e)
        {
            if (FactoryCount > 0)
            {
                FactoryTime += 1;

                if (FactoryTime >= FactoryCooldown)
                {
                    FactoryTime = 0;
                    CookieCount += FactoryCount * FactoryMake;
                }

            }

            if (MineCount > 0)
            {
                MineTime += 1;

                if (MineTime > MineCooldown)
                {
                    MineTime = 0;
                    CookieCount += MineCount * MineMake;
                }

            }

            if (PortalCount > 0)
            {
                PortalTime += 1;

                if (PortalTime > PortalCooldown)
                {
                    PortalTime = 0;
                    CookieCount += PortalCount * PortalMake;
                }

            }


            UpdateWindow();
        }

    }
}
